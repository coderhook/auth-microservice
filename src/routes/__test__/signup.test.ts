import request from "supertest";
import { app } from "../../app";

test("returns 201 on successful signup", async () => {
  return request(app)
    .post("/api/users/signup")
    .send({
      email: "test@test.com",
      password: "password",
    })
    .expect(201);
});

test("return 400 with and invalid email", async () => {
  return request(app)
    .post("/api/users/signup")
    .send({
      email: "testInvalidEmail",
      password: "password",
    })
    .expect(400);
});

test("return 400 with and invalid password", async () => {
  return request(app)
    .post("/api/users/signup")
    .send({
      email: "test@valid.com",
      password: "p",
    })
    .expect(400);
});

test("return 400 with and empty email or password", async () => {
  await request(app)
    .post("/api/users/signup")
    .send({ email: "test@valid.com" })
    .expect(400);
  await request(app)
    .post("/api/users/signup")
    .send({ password: "validPassword" })
    .expect(400);
});

test("disallows duplicate emails", async () => {
  await request(app)
    .post("/api/users/signup")
    .send({
      email: "test@test.com",
      password: "password",
    })
    .expect(201);

  await request(app)
    .post("/api/users/signup")
    .send({
      email: "test@test.com",
      password: "password",
    })
    .expect(400);
});

test("sets a cookie after successful signup", async () => {
  const response = await request(app)
    .post("/api/users/signup")
    .send({
      email: "test@test.com",
      password: "password",
    })
    .expect(201);

  expect(response.get("Set-Cookie")).toBeDefined();
});
